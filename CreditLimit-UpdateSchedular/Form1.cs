using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Configuration;

namespace CreditLimit_UpdateSchedular
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //string localconnection = ConfigurationManager.AppSettings["localConnectString"];
        string oracleconn = ConfigurationManager.AppSettings["oracleConnection"];
        string Instaconn = ConfigurationManager.AppSettings["corConnectString"];

        private void Form1_Load(object sender, EventArgs e)
        {
            string DayName;
            DayName = System.DateTime.Now.DayOfWeek.ToString();

            InsertInvoiceDetails(); //CRD Oracle Upload scheduler
            //UploadCarHireDetailsInOracle();
            // GetOracleVendorNameAndOracleCode();
            // GetOracleTotalcost(); //check totalcost in oracle
            //UploadCarHireDetailsInOracleForProvision();

            //credit limit scheduler check start **********************
            /*
            int statustest = 0;

            statustest = GetCreditLimitNUpdateOutstanding(); //Get Oracle Outstanding
            if (statustest == 0)
            {
                statustest = GetCreditLimitNUpdateOutstanding(); //Get Oracle Outstanding
            }
            if (statustest == 1)
            {
                SendCreditLimitMailNew(); //new mailer with body changes

                BlockUnblock(); //Block / Unblock Client
            }
            */
            //credit limit scheduler check end   **********************

            //if (1 == 1)
            if (1 != 1)
            {
                if (DayName != "Saturday" && DayName != "Sunday")
                {
                    if (System.DateTime.Now.Hour == 9 || System.DateTime.Now.Hour == 10)
                    {
                        int status = 0;

                        status = GetCreditLimitNUpdateOutstanding(); //Get Oracle Outstanding
                        if (status == 0)
                        {
                            status = GetCreditLimitNUpdateOutstanding(); //Get Oracle Outstanding
                        }
                        if (status == 1)
                        {
                            //SendCreditLimitMail(); //send Outstanding Mail //old mailer with out body changes
                            if (System.DateTime.Now.Hour == 10)
                            {
                                SendCreditLimitMailNew(); //new mailer with body changes

                                BlockUnblock(); //Block / Unblock Client
                            }
                        }

                        Application.Exit();
                        return;
                    }

                    if (System.DateTime.Now.Hour == 19)
                    {
                        BlockUnblock(); //Block / Unblock Client
                    }

                    if (DayName == "Tuesday" || DayName == "Thursday")
                    {
                        if (System.DateTime.Now.Hour == 12)
                        {
                            SendCreditLimitMail_Client();
                            Application.Exit();
                            return;
                        }
                    }
                }

                if (System.DateTime.Now.Hour == 23)
                {
                    if (System.DateTime.Now.Day == 7 || System.DateTime.Now.Day == 14 || System.DateTime.Now.Day == 16 || System.DateTime.Now.Day == 21
                        || System.DateTime.Now.Day == System.DateTime.DaysInMonth(System.DateTime.Now.Year, System.DateTime.Now.Month))
                    {
                        UpdateOracleCarCode(); //scheduler set to update oracle car code every day
                    }
                }

                if (System.DateTime.Now.Hour == 5)
                {
                    InsertInvoiceDetails(); //every day runs except on sunday
                    //InsertInvoiceDetailsMyles(); //every day runs except on sunday

                    UpdateOracleClientNameInInsta();

                    if (DayName == "Saturday")
                    {
                        UpdateOraTable(); //schedular set to run at 5 AM every saturday
                    }
                }

                if (System.DateTime.Now.Day == 1 && System.DateTime.Now.Hour == 22)
                {
                    UpdateOraTable(); //schedular set to run at 5 AM every saturday
                }
            }
            Application.Exit();
        }

        //****************NEW Credit Limit ********************************************************
        public int GetCreditLimitNUpdateOutstanding()
        {
            string Error = "";
            int status = 0;
            try
            {
                OracleConnection Oraconnect = new OracleConnection(oracleconn);
                Oraconnect.Open();

                SqlConnection InstaConnection = new SqlConnection(Instaconn);
                InstaConnection.Open();

                //SqlConnection localSqlCon = new SqlConnection(localconnection);
                //localSqlCon.Open();

                string ViewUpdate = "DBMS_APPLICATION_INFO.SET_CLIENT_INFO(82)";
                OracleCommand cmdStart = new OracleCommand(ViewUpdate, Oraconnect);
                cmdStart.CommandType = CommandType.StoredProcedure;
                cmdStart.ExecuteNonQuery();


                //SqlCommand getInstadetail = new SqlCommand("prc_NewCreditLimitWithBlock_N_Unblock", InstaConnection);
                SqlCommand getInstadetail = new SqlCommand("prc_NewCreditLimitWithBlock_N_Unblock_New", InstaConnection);
                getInstadetail.CommandTimeout = 18000;
                SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
                DataSet dsInstadetail = new DataSet();

                dbInstadetail.Fill(dsInstadetail);
                if (dsInstadetail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsInstadetail.Tables[0].Rows.Count; i++)
                    {
                        string OracleCompanyName = null;
                        //mapping table query start
                        //StringBuilder sqlGetInstaCompanyName = new StringBuilder("select * from auto_import where RMS_Cust = '" + dsInstadetail.Tables[0].Rows[i]["ClientCoName"].ToString().Replace("'", "''") + "'");
                        //SqlCommand cmdGetInstaCompanyName = new SqlCommand(sqlGetInstaCompanyName.ToString(), localSqlCon);
                        //SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmdGetInstaCompanyName);
                        //DataSet sqlDataset = new DataSet();
                        //sqlAdapter.Fill(sqlDataset);
                        ////End Getting Insta Company Name from Mapping Table
                        //if (sqlDataset.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int a = 0; a < sqlDataset.Tables[0].Rows.Count; a++)
                        //    {
                        //        OracleCompanyName = sqlDataset.Tables[0].Rows[a]["CIPL_CUST"].ToString().Replace("'", "''");
                        //    }
                        //}
                        //else
                        //{
                        //    OracleCompanyName = "";
                        //}
                        OracleCompanyName = dsInstadetail.Tables[0].Rows[i]["ClientCoName"].ToString().Replace("'", "''");
                        //mapping table query end


                        if (OracleCompanyName != "" && dsInstadetail.Tables[0].Rows[i]["ModifiedDate"].ToString() == "")
                        {

                            double OracleOutstanding = 0, OracleOutstanding90 = 0, OracleOutstanding120 = 0
                                , OracleOutstanding150 = 0, OracleOutstanding180 = 0, OracleOutstanding365 = 0
                                , OracleOutstandingLastMonth = 0, OracleOutstandingUNAPPLIED_LASTDAY = 0
                                , OracleOutstandingCollectionLastMonth = 0, OracleOutstandingCollectionCurrentMonth = 0;


                            if (dsInstadetail.Tables[0].Rows[i]["PaymentTerms"].ToString() == "Cr")  //payment terms code start
                            {

                                try
                                {
                                    //total outstanding start
                                    //StringBuilder OraOutstanding = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from CUSTOMER_OUTSTANDING_V ");
                                    //OraOutstanding.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding = new StringBuilder(" select CUSTOMER_OUTSTANDING_V('");
                                    OraOutstanding.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");
                                    OracleCommand searchoracle = new OracleCommand(OraOutstanding.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter = new OracleDataAdapter(searchoracle);

                                    DataSet OraDataset = new DataSet();

                                    oraAdapter.Fill(OraDataset);
                                    if (OraDataset.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding = Convert.ToDouble(OraDataset.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding = 0;
                                    }
                                    //total outstanding end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }


                                try
                                {
                                    //CUSTOMER UNAPPLIED LASTDAY start
                                    StringBuilder OraOutstandingUNAPPLIED_LASTDAY = new StringBuilder(" select CUSTOMER_UNAPPLIED_LASTDAY('");
                                    OraOutstandingUNAPPLIED_LASTDAY.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual ");

                                    OracleCommand searchoracleUNAPPLIED_LASTDAY = new OracleCommand(OraOutstandingUNAPPLIED_LASTDAY.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapterUNAPPLIED_LASTDAY = new OracleDataAdapter(searchoracleUNAPPLIED_LASTDAY);
                                    DataSet OraDatasetUNAPPLIED_LASTDAY = new DataSet();

                                    oraAdapterUNAPPLIED_LASTDAY.Fill(OraDatasetUNAPPLIED_LASTDAY);
                                    if (OraDatasetUNAPPLIED_LASTDAY.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDatasetUNAPPLIED_LASTDAY.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDatasetUNAPPLIED_LASTDAY.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstandingUNAPPLIED_LASTDAY = Convert.ToDouble(OraDatasetUNAPPLIED_LASTDAY.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstandingUNAPPLIED_LASTDAY = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstandingUNAPPLIED_LASTDAY = 0;
                                    }
                                    //CUSTOMER UNAPPLIED LASTDAY end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }


                                try
                                {
                                    //outstanding Last Month start
                                    StringBuilder OraOutstandingLastMonth = new StringBuilder(" select CUSTOMER_OUTSTANDING_LASTDAY('");
                                    OraOutstandingLastMonth.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual ");

                                    OracleCommand searchoracleLastMonth = new OracleCommand(OraOutstandingLastMonth.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapterLastMonth = new OracleDataAdapter(searchoracleLastMonth);
                                    DataSet OraDatasetLastMonth = new DataSet();

                                    oraAdapterLastMonth.Fill(OraDatasetLastMonth);
                                    if (OraDatasetLastMonth.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDatasetLastMonth.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDatasetLastMonth.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstandingLastMonth = Convert.ToDouble(OraDatasetLastMonth.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstandingLastMonth = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstandingLastMonth = 0;
                                    }
                                    //outstanding Last Month end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }

                                try
                                {
                                    //outstanding 90 start
                                    //StringBuilder OraOutstanding90 = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from OUTSTANDING_90 ");
                                    //OraOutstanding90.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding90 = new StringBuilder(" select OUTSTANDING_90('");
                                    OraOutstanding90.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");

                                    OracleCommand searchoracle90 = new OracleCommand(OraOutstanding90.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter90 = new OracleDataAdapter(searchoracle90);
                                    DataSet OraDataset90 = new DataSet();

                                    oraAdapter90.Fill(OraDataset90);
                                    if (OraDataset90.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset90.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset90.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding90 = Convert.ToDouble(OraDataset90.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding90 = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding90 = 0;
                                    }
                                    //outstanding 90 end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }

                                try
                                {
                                    //outstanding 120 start
                                    //StringBuilder OraOutstanding120 = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from OUTSTANDING_120 ");
                                    //OraOutstanding120.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding120 = new StringBuilder("select OUTSTANDING_120('");
                                    OraOutstanding120.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");

                                    OracleCommand searchoracle120 = new OracleCommand(OraOutstanding120.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter120 = new OracleDataAdapter(searchoracle120);
                                    DataSet OraDataset120 = new DataSet();

                                    oraAdapter120.Fill(OraDataset120);
                                    if (OraDataset120.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset120.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset120.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding120 = Convert.ToDouble(OraDataset120.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding120 = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding120 = 0;
                                    }
                                    //outstanding 120 end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }

                                try
                                {
                                    //outstanding 150 start
                                    //StringBuilder OraOutstanding150 = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from OUTSTANDING_150 ");
                                    //OraOutstanding150.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding150 = new StringBuilder("select OUTSTANDING_150('");
                                    OraOutstanding150.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");

                                    OracleCommand searchoracle150 = new OracleCommand(OraOutstanding150.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter150 = new OracleDataAdapter(searchoracle150);
                                    DataSet OraDataset150 = new DataSet();

                                    oraAdapter150.Fill(OraDataset150);
                                    if (OraDataset150.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset150.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset150.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding150 = Convert.ToDouble(OraDataset150.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding150 = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding150 = 0;
                                    }
                                    //outstanding 150 end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }


                                try
                                {
                                    //outstanding 180 start
                                    //StringBuilder OraOutstanding180 = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from OUTSTANDING_180 ");
                                    //OraOutstanding180.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding180 = new StringBuilder(" select OUTSTANDING_180('");
                                    OraOutstanding180.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");

                                    OracleCommand searchoracle180 = new OracleCommand(OraOutstanding180.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter180 = new OracleDataAdapter(searchoracle180);
                                    DataSet OraDataset180 = new DataSet();

                                    oraAdapter180.Fill(OraDataset180);
                                    if (OraDataset180.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset180.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset180.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding180 = Convert.ToDouble(OraDataset180.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding180 = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding180 = 0;
                                    }
                                    //outstanding 180 end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }

                                try
                                {
                                    //outstanding 365 start
                                    //StringBuilder OraOutstanding365 = new StringBuilder(" select sum(AMOUNT_DUE_REMAINING) as AMOUNT_DUE_REMAINING from OUTSTANDING_365 ");
                                    //OraOutstanding365.Append(" where replace(CUSTOMER_NAME,'''','') = '" + OracleCompanyName + "'");

                                    StringBuilder OraOutstanding365 = new StringBuilder("select OUTSTANDING_365('");
                                    OraOutstanding365.Append(OracleCompanyName + "') AMOUNT_DUE_REMAINING from dual");

                                    OracleCommand searchoracle365 = new OracleCommand(OraOutstanding365.ToString(), Oraconnect);
                                    OracleDataAdapter oraAdapter365 = new OracleDataAdapter(searchoracle365);
                                    DataSet OraDataset365 = new DataSet();

                                    oraAdapter365.Fill(OraDataset365);
                                    if (OraDataset365.Tables[0].Rows.Count > 0)
                                    {
                                        for (int k = 0; k < OraDataset365.Tables[0].Rows.Count; k++)
                                        {
                                            if (OraDataset365.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"].ToString() != "")
                                            {
                                                OracleOutstanding365 = Convert.ToDouble(OraDataset365.Tables[0].Rows[k]["AMOUNT_DUE_REMAINING"]);
                                            }
                                            else
                                            {
                                                OracleOutstanding365 = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OracleOutstanding365 = 0;
                                    }
                                    //outstanding 365 end
                                }
                                catch (Exception ex)
                                {
                                    Error = Error + ";" + OracleCompanyName;
                                }

                            }  //payment terms logic end


                            try
                            {
                                //********************************************colletion code start 
                                //collection last month start

                                StringBuilder OraCollectionLastMonth = new StringBuilder("select F_COLLECTION_LAST_MONTH('");
                                OraCollectionLastMonth.Append(OracleCompanyName + "') CollectionAmt from dual");

                                OracleCommand searchoracleCollectionLastMonth = new OracleCommand(OraCollectionLastMonth.ToString(), Oraconnect);
                                OracleDataAdapter oraAdapterCollectionLastMonth = new OracleDataAdapter(searchoracleCollectionLastMonth);
                                DataSet OraDatasetCollectionLastMonth = new DataSet();

                                oraAdapterCollectionLastMonth.Fill(OraDatasetCollectionLastMonth);
                                if (OraDatasetCollectionLastMonth.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < OraDatasetCollectionLastMonth.Tables[0].Rows.Count; k++)
                                    {
                                        if (OraDatasetCollectionLastMonth.Tables[0].Rows[k]["CollectionAmt"].ToString() != "")
                                        {
                                            OracleOutstandingCollectionLastMonth = Convert.ToDouble(OraDatasetCollectionLastMonth.Tables[0].Rows[k]["CollectionAmt"]);
                                        }
                                        else
                                        {
                                            OracleOutstandingCollectionLastMonth = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    OracleOutstandingCollectionLastMonth = 0;
                                }
                                //collection last month end
                            }
                            catch (Exception ex)
                            {
                                Error = Error + ";" + OracleCompanyName;
                            }

                            try
                            {
                                //collection Current month start

                                StringBuilder OraCollectionCurrentMonth = new StringBuilder("select F_COLLECTION_CURRENT_MONTH('");
                                OraCollectionCurrentMonth.Append(OracleCompanyName + "') CollectionAmt from dual");

                                OracleCommand searchoracleCollectionCurrentMonth = new OracleCommand(OraCollectionCurrentMonth.ToString(), Oraconnect);
                                OracleDataAdapter oraAdapterCollectionCurrentMonth = new OracleDataAdapter(searchoracleCollectionCurrentMonth);
                                DataSet OraDatasetCollectionCurrentMonth = new DataSet();

                                oraAdapterCollectionCurrentMonth.Fill(OraDatasetCollectionCurrentMonth);
                                if (OraDatasetCollectionCurrentMonth.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < OraDatasetCollectionCurrentMonth.Tables[0].Rows.Count; k++)
                                    {
                                        if (OraDatasetCollectionCurrentMonth.Tables[0].Rows[k]["CollectionAmt"].ToString() != "")
                                        {
                                            OracleOutstandingCollectionCurrentMonth = Convert.ToDouble(OraDatasetCollectionCurrentMonth.Tables[0].Rows[k]["CollectionAmt"]);
                                        }
                                        else
                                        {
                                            OracleOutstandingCollectionCurrentMonth = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    OracleOutstandingCollectionCurrentMonth = 0;
                                }
                                //collection Current month end

                                //********************************************collection code end
                            }
                            catch (Exception ex)
                            {
                                Error = Error + ";" + OracleCompanyName;
                            }



                            //update outstanding in table start
                            //
                            //string UpdateOutstanding = "exec Prc_UpdateOutstanding_New1 " + OracleOutstanding90
                            string UpdateOutstanding = "exec Prc_UpdateOutstanding_ClientID " + OracleOutstanding90
                                + "," + OracleOutstanding120 + "," + OracleOutstanding150
                                + "," + OracleOutstanding180 + "," + OracleOutstanding365
                                + "," + OracleOutstanding + "," + OracleOutstandingLastMonth
                                + "," + OracleOutstandingUNAPPLIED_LASTDAY
                                + "," + OracleOutstandingCollectionLastMonth
                                + "," + OracleOutstandingCollectionCurrentMonth
                                + ",'" + dsInstadetail.Tables[0].Rows[i]["ClientCoName"].ToString().Replace("'", "''") + "'";
                            SqlCommand cmdupdateOutstanding = new SqlCommand(UpdateOutstanding, InstaConnection);
                            //cmdupdateOutstanding.CommandType = CommandType.StoredProcedure;
                            cmdupdateOutstanding.ExecuteNonQuery();
                            //update outstanding in table end

                        } //if (OracleCompanyName != "")
                        else
                        {
                            if (dsInstadetail.Tables[0].Rows[i]["ModifiedDate"].ToString() == "")
                            {
                                //update outstanding in table start
                                string UpdateOutstanding = "exec Prc_UpdateOutstanding_ClientID 0, 0, 0, 0, 0, 0, 0, 0, 0, 0"
                                    + ",'" + dsInstadetail.Tables[0].Rows[i]["ClientCoName"].ToString().Replace("'", "''") + "'";
                                SqlCommand cmdupdateOutstanding = new SqlCommand(UpdateOutstanding, InstaConnection);
                                //cmdupdateOutstanding.CommandType = CommandType.StoredProcedure;
                                cmdupdateOutstanding.ExecuteNonQuery();
                                //update outstanding in table end
                            }
                        }
                    }
                }

                cmdStart.Dispose(); //dispose view query of oracle
                Oraconnect.Close();
                InstaConnection.Close();
                //localSqlCon.Close();
                status = 1;
            }
            catch (Exception Ex)
            {
                //MessageBox.Show(Ex.Message.ToString());
                status = 0;
                return status;
            }
            return status;
        }

        public void SendCreditLimitMail_Client()
        {
            SqlConnection InstaConnection = new SqlConnection(Instaconn);

            if (InstaConnection.State == ConnectionState.Closed)
            {
                InstaConnection.Open();
            }

            try
            {

                StringBuilder strAttachment = new StringBuilder();
                SqlCommand cmdSummary = new SqlCommand("exec Prc_CreditLimitMail_Client ''", InstaConnection); //added on 11-Nov-2014
                SqlDataAdapter daSummary = new SqlDataAdapter();
                daSummary.SelectCommand = cmdSummary;
                DataTable dtEx = new DataTable();
                daSummary.Fill(dtEx);

                string filename = null, EmailID = null;

                for (int i = 0; i < dtEx.Rows.Count; i++)
                {
                    if (dtEx.Rows[i]["billsubmissionpersonemail"].ToString().Trim() != "")
                    {
                        EmailID = dtEx.Rows[i]["EmailID"].ToString().Trim() + "," + dtEx.Rows[i]["billsubmissionpersonemail"].ToString().Trim();
                    }
                    else
                    {
                        EmailID = dtEx.Rows[i]["EmailID"].ToString().Trim();
                    }
                    //EmailID = "abd@abd.com";

                    filename = dtEx.Rows[i]["ClientCoName"].ToString().Replace("'", "''");
                    filename = filename.Replace("/", "");
                    filename = filename.Replace(":", "");
                    filename = filename.Replace("\\", "");
                    filename = filename.Replace("*", "");
                    filename = filename.Replace("?", "");
                    filename = filename.Replace("<", "");
                    filename = filename.Replace(">", "");
                    filename = filename.Replace("|", "");
                    filename = filename.Replace("\"", "");

                    StringBuilder strBody = new StringBuilder();

                    strBody.Append("<html xmlns='http://www.w3.org/1999/xhtml' >");
                    strBody.Append("To,<br/><br/>");
                    strBody.Append(filename + ", " + dtEx.Rows[i]["CityName"].ToString() + "<br/><br/>");
                    strBody.Append("Dear Sir / Madam,<br/><br/>");
                    strBody.Append("Greetings from Carzonrent……!");
                    strBody.Append("<br/><br/>");
                    strBody.Append("We would like to draw your kind attention towards low credit limit available");
                    strBody.Append(" towards your account with us due to delay in payment / non- payment of overdue outstanding invoices.");

                    strBody.Append("<br/><br/>");
                    strBody.Append("<table>");

                    strBody.Append("<tr>");
                    strBody.Append("<td>");
                    strBody.Append("Available Credit Limit");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    strBody.Append(":");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    if (Convert.ToDouble(dtEx.Rows[i]["AvailableBalance"]) > 0)
                    {
                        strBody.Append("&nbsp;Rs. " + Convert.ToDouble(dtEx.Rows[i]["AvailableBalance"]).ToString() + "<br/>");
                    }
                    else
                    {
                        strBody.Append("&nbsp;Rs. 0.00<br/>");
                    }
                    strBody.Append("</td>");
                    strBody.Append("</tr>");

                    strBody.Append("<tr>");
                    strBody.Append("<td>");
                    strBody.Append("Outstanding invoices beyond 90 days");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    strBody.Append(":");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    if (Convert.ToDouble(dtEx.Rows[i]["Outstanding90Day"]) <= 0)
                    {
                        strBody.Append("&nbsp;Rs. 0.00 <br/>");
                    }
                    else
                    {
                        strBody.Append("&nbsp;Rs. " + Convert.ToDouble(dtEx.Rows[i]["Outstanding90Day"]).ToString() + "<br/>");
                    }
                    strBody.Append("</td>");
                    strBody.Append("</tr>");

                    DateTime createDate;
                    createDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1);

                    strBody.Append("<tr>");
                    strBody.Append("<td>");
                    strBody.Append("Outstanding upto " + createDate.ToString("dd/MMM/yyyy") + " is ");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    strBody.Append(":");
                    strBody.Append("</td>");
                    strBody.Append("<td>");
                    if (Convert.ToDouble(dtEx.Rows[i]["OutstandingLastMonth"]) <= 0)
                    {
                        strBody.Append("&nbsp;Null");
                    }
                    else
                    {
                        strBody.Append("&nbsp;Rs. " + Convert.ToDouble(dtEx.Rows[i]["OutstandingLastMonth"]).ToString());
                    }
                    strBody.Append(" as on dated " + DateTime.Now.ToString("dd/MMM/yyyy") + " as per our records.<br/>");
                    strBody.Append("</td>");
                    strBody.Append("</tr>");
                    strBody.Append("</table>");
                    strBody.Append("<br/><br/>");
                    strBody.Append("We request you to make immediate payment of Outstanding invoices more than 90 days and processing of all outstanding invoices within agreed credit period.");
                    strBody.Append("<br/><br/>");
                    strBody.Append("You may reach out to the relationship Manager allocated to your corporate for any further assistance.");
                    strBody.Append("<br/><br/>");
                    strBody.Append("With Regards,");
                    strBody.Append("<br/>");
                    strBody.Append("Credit Control Team");
                    strBody.Append("<br/>");
                    strBody.Append("Carzonrent");
                    strBody.Append("<br/><br/><br/><br/>");
                    strBody.Append("*Note : This is system generated mail. Please do not reply and in case of any deviation, please get a touch with the relationship Manager. The mentioned amount may vary based upon your entities or service locations.");

                    string Subject = string.Empty;
                    Subject = "Critical available Credit Limit – Car Rental Services";
                    sendMail(filename, strBody.ToString(), strAttachment.ToString(), EmailID, Subject);
                }

                InstaConnection.Close();

            }
            catch (Exception ex)
            {
                InstaConnection.Close();
                //MessageBox.Show(ex.Message.ToString());
            }
        }

        private void sendMail(string FileName, string mailContent, string FileContent
           , string EmailID, string Subject)
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                string From = "CreditControl@carzonrent.com";
                message.To.Add(EmailID);
                message.CC.Add("creditcor@carzonrent.com");
                message.Bcc.Add("techreports@carzonrent.com");

                message.Subject = Subject;
                message.From = new System.Net.Mail.MailAddress(From);
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;
                message.Body = mailContent.ToString();
                message.ReplyTo = new System.Net.Mail.MailAddress("creditcor@carzonrent.com");
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("CreditControl@carzonrent.com", "Credit@1234");
                smtp.Send(message);
                Thread.Sleep(1000);
                //MessageBox.Show("Send");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                //if (EmailID != "ajay.mishra@carzonrent.com")
                //{
                //    sendMail(FileName, mailContent, FileContent
                //    , "ajay.mishra@carzonrent.com", "Delivery Failure for emailid : " + EmailID);
                //}
            }
        }

        public void SendCreditLimitMail() //not used
        {
            SqlConnection InstaConnection = new SqlConnection(Instaconn);

            if (InstaConnection.State == ConnectionState.Closed)
            {
                InstaConnection.Open();
            }

            try
            {
                StringBuilder strBody = new StringBuilder();
                StringBuilder strAttachment = new StringBuilder();
                SqlCommand cmdSummary = new SqlCommand("exec Prc_CreditLimitMail_New ''", InstaConnection);
                SqlDataAdapter daSummary = new SqlDataAdapter();
                daSummary.SelectCommand = cmdSummary;
                DataTable dtEx = new DataTable();
                daSummary.Fill(dtEx);

                strBody.Append("<html xmlns='http://www.w3.org/1999/xhtml' >");
                strBody.Append("Dear Team,<br/><br/>");
                strBody.Append("The attached <b><u>list of clients</u></b>  are having details of period wise outstanding in your  ");
                strBody.Append("respective locations. The clients having low credit availability may get <b><u>BLOCKED</u></b> by the system any time, due to non-collection of dues.<br/><br/>");
                strBody.Append("You are being advised to immediately take action to collect / update the payment in  ");
                strBody.Append("the system enabling you to keep credit limit available for accepting of reservations.<br/><br/>");
                strBody.Append("You are required to ensure clearances of unpaid invoices in the greater than <b><u>90 days bucket</u></b>. ");
                strBody.Append("In case the outstanding / unpaid invoices reaches to the 120th days, the ");
                strBody.Append("account will get blocked as per the Credit Policy. You would also need to knock off ");
                strBody.Append("amounts appearing under unapplied credit correctly in the system without any ");
                strBody.Append("further reminders. Any short payment / deductions must be properly booked in ");
                strBody.Append("system on regular basis.<br/><br/>");
                strBody.Append("The detailed list of outstanding invoices can be acquired from branch accounts.<br/><br/>");
                strBody.Append("<b><u>The below category of clients are marked in the attachment :</u></b><br/><br/>");
                strBody.Append("Exception        : Clients, which are exempted from automated credit limit process.<br />");
                strBody.Append("Category A       : Clients having More than credit Limit of Rs 3,00,000/-.<br />");
                strBody.Append("Category B       : Clients having credit limit of Rs 3,00,000/- or less. These clients get ");
                strBody.Append("blocked in case any invoice remains pending for payment beyond 120 days. System ");
                strBody.Append("generated email goes to such clients in case they use their 70% of credit limit or any ");
                strBody.Append("invoice remains pending for payment beyond 90 days.<br />");
                strBody.Append("New Clients    : Clients, which are out of automated credit limit for initial 2 months ");
                strBody.Append("from date of creation and such clients carry credit limit as per CAM for the interim ");
                strBody.Append("period.<br /><br />");
                strBody.Append("For any clarification, you may reach out to Mr. Tej Prakash (M : 9971399315, tej.prakash@carzonrent.com)<br/><br/>");
                //strBody.Append("Regards,<br/><br/>");
                //strBody.Append("Credit Control Team<br/><br/><br/><br/>");
                //strBody.Append("This is system generated mail. Please do not reply.");
                //strBody.Append("</body></html>");

                strAttachment = new StringBuilder("<table border = 1 align = 'center'  width = 100%> ");
                if (dtEx.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEx.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            strAttachment.Append("<tr>");
                            for (int j = 0; j < dtEx.Columns.Count; j++)
                            {
                                if (dtEx.Columns[j].Caption.ToString() == "ClientCoName")
                                {
                                    strAttachment.Append("<td align='center'><b>Client Name</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "CityName")
                                {
                                    strAttachment.Append("<td align='center'><b>City Name</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "CreditLimitToUpdate")
                                {
                                    strAttachment.Append("<td align='center'><b>Credit Limit (INR)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding (Including Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "BalanceLimit")
                                {
                                    strAttachment.Append("<td align='center'><b>Balance Limit</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding90Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 90 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding120Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 120 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding150Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 150 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding180Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 180 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding365Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 365 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Status")
                                {
                                    strAttachment.Append("<td align='center'><b>Status</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Category")
                                {
                                    strAttachment.Append("<td align='center'><b>Category</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "OutstandingLastMonth")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding Last Month (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "OutstandingUNAPPLIED_LASTDAY")
                                {
                                    strAttachment.Append("<td align='center'><b>Unapplied Collections</b></td>");
                                }
                            }
                            strAttachment.Append("</tr>");
                        }
                        strAttachment.Append("<tr>");

                        for (int j = 0; j < dtEx.Columns.Count; j++)
                        {
                            if (dtEx.Columns[j].Caption.ToString() != "BodyYN")
                            {
                                strAttachment.Append("<td align='center'>" + dtEx.Rows[i][j].ToString() + "</td>");
                            }
                        }
                        strAttachment.Append("</tr>");
                    }

                    strAttachment.Append("</table>");

                    string filename = null;

                    filename = "CreditLimit_" + System.DateTime.Now.Year.ToString()
                        + "_" + System.DateTime.Now.Month.ToString()
                        + "_" + System.DateTime.Now.Day.ToString();

                    sendMailNew(filename, strBody.ToString(), strAttachment.ToString(), "CreditControlEmail@carzonrent.com");
                }

                strBody.Append("Regards,<br/><br/>");
                strBody.Append("Credit Control Team<br/><br/><br/><br/>");
                strBody.Append("This is system generated mail. Please do not reply.");
                strBody.Append("</body></html>");

                InstaConnection.Close();
            }
            catch (Exception ex)
            {
                InstaConnection.Close();
                //MessageBox.Show(ex.Message.ToString());
            }
        }

        public void SendCreditLimitMailNew()
        {
            SqlConnection InstaConnection = new SqlConnection(Instaconn);

            if (InstaConnection.State == ConnectionState.Closed)
            {
                InstaConnection.Open();
            }

            try
            {
                StringBuilder strBody = new StringBuilder();
                StringBuilder strAttachment = new StringBuilder();
                SqlCommand cmdSummary = new SqlCommand("exec Prc_CreditLimitMail_New ''", InstaConnection);
                SqlDataAdapter daSummary = new SqlDataAdapter();
                daSummary.SelectCommand = cmdSummary;
                DataTable dtEx = new DataTable();
                daSummary.Fill(dtEx);

                strBody.Append("<html xmlns='http://www.w3.org/1999/xhtml' >");
                strBody.Append("<body>");
                strBody.Append("Dear Team,<br/><br/>");
                strBody.Append("The attached <b><u>list of clients</u></b>  are having details of period wise outstanding in your  ");
                strBody.Append("respective locations. <br/><br/>");
                strBody.Append("Also <br/><br/>");
                strBody.Append("Find <b><u>below the list of clients</u></b> having low credit availability may get <b><u>BLOCKED</u></b> by the system any time, due to non-collection of dues.<br/><br/>");
                strBody.Append("You are being advised to immediately take action to collect / update the payment in  ");
                strBody.Append("the system enabling you to keep credit limit available for accepting of reservations.<br/><br/>");
                strBody.Append("You are required to ensure clearances of unpaid invoices in the greater than <b><u>90 days bucket</u></b>. ");
                strBody.Append("In case the outstanding / unpaid invoices reaches to the 120th days, the ");
                strBody.Append("account will get blocked as per the Credit Policy. You would also need to knock off ");
                strBody.Append("amounts appearing under unapplied credit correctly in the system without any ");
                strBody.Append("further reminders. Any short payment / deductions must be properly booked in ");
                strBody.Append("system on regular basis.<br/><br/>");
                strBody.Append("The detailed list of outstanding invoices can be acquired from branch accounts.<br/><br/>");


                strBody.Append("<table border = 1 align = 'center'  width = 100%>");
                //strBody.Append("Regards,<br/><br/>");
                //strBody.Append("Credit Control Team<br/><br/><br/><br/>");
                //strBody.Append("This is system generated mail. Please do not reply.");
                //strBody.Append("</body></html>");

                strAttachment = new StringBuilder("<table border = 1 align = 'center'  width = '100%'> ");
                if (dtEx.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEx.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            strAttachment.Append("<tr>");
                            strBody.Append("<tr>");
                            for (int j = 0; j < dtEx.Columns.Count; j++)
                            {
                                if (dtEx.Columns[j].Caption.ToString() == "ClientCoName")
                                {
                                    strAttachment.Append("<td align='center'><b>Client Name</b></td>");
                                    strBody.Append("<td align='center'><b>Client Name</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "CityName")
                                {
                                    strAttachment.Append("<td align='center'><b>City Name</b></td>");
                                    strBody.Append("<td align='center'><b>City Name</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "CreditLimitToUpdate")
                                {
                                    strAttachment.Append("<td align='center'><b>Credit Limit (INR)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding (Including Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding (Including Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "BalanceLimit")
                                {
                                    strAttachment.Append("<td align='center'><b>Balance Limit</b></td>");
                                    strBody.Append("<td align='center'><b>Balance Limit</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding90Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 90 Days (Excluding Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding 90 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding120Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 120 Days (Excluding Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding 120 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding150Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 150 Days (Excluding Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding 150 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding180Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 180 Days (Excluding Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding 180 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Outstanding365Day")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding 365 Days (Excluding Unapplied)</b></td>");
                                    strBody.Append("<td align='center'><b>Outstanding 365 Days (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Status")
                                {
                                    strAttachment.Append("<td align='center'><b>Status</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "Category")
                                {
                                    strAttachment.Append("<td align='center'><b>Category</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "OutstandingLastMonth")
                                {
                                    strAttachment.Append("<td align='center'><b>Outstanding Last Month (Excluding Unapplied)</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "OutstandingUNAPPLIED_LASTDAY")
                                {
                                    strAttachment.Append("<td align='center'><b>Unapplied Collections</b></td>");
                                    strBody.Append("<td align='center'><b>Unapplied Collections</b></td>");
                                }
                                if (dtEx.Columns[j].Caption.ToString() == "ContractEndDate")
                                {
                                    strAttachment.Append("<td align='center'><b>Contract End Date</b></td>");
                                }
                            }
                            strAttachment.Append("</tr>");
                            strBody.Append("</tr>");
                        }
                        strAttachment.Append("<tr>");


                        bool BodyYN = false;
                        bool ClientStatus = false;
                        for (int j = 0; j < dtEx.Columns.Count; j++)
                        {

                            if (dtEx.Columns[j].Caption.ToString() != "BodyYN" && dtEx.Columns[j].Caption.ToString() != "Status1")
                            {
                                if (dtEx.Columns[j].Caption.ToString() == "ContractEndDate")
                                {
                                    strAttachment.Append("<td align='center'>" + Convert.ToDateTime(dtEx.Rows[i][j]).ToShortDateString() + "</td>");
                                }
                                else
                                {
                                    strAttachment.Append("<td align='center'>" + dtEx.Rows[i][j].ToString() + "</td>");
                                }
                            }
                            else
                            {
                                if (dtEx.Columns[j].Caption.ToString() == "Status1")
                                {
                                    if (dtEx.Rows[i][j].ToString() == "Active")
                                    {
                                        ClientStatus = true;
                                    }
                                    else
                                    {
                                        ClientStatus = false;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToBoolean(dtEx.Rows[i][j]) && ClientStatus)
                                    {
                                        BodyYN = true;
                                    }
                                    else
                                    {
                                        BodyYN = false;
                                    }
                                }
                            }

                            if (BodyYN)
                            {
                                if (dtEx.Columns[j].Caption.ToString() == "ClientCoName" || dtEx.Columns[j].Caption.ToString() == "CityName"
                                    || dtEx.Columns[j].Caption.ToString() == "Outstanding" || dtEx.Columns[j].Caption.ToString() == "BalanceLimit"
                                    || dtEx.Columns[j].Caption.ToString() == "Outstanding90Day" || dtEx.Columns[j].Caption.ToString() == "Outstanding120Day"
                                    || dtEx.Columns[j].Caption.ToString() == "Outstanding150Day" || dtEx.Columns[j].Caption.ToString() == "Outstanding180Day"
                                    || dtEx.Columns[j].Caption.ToString() == "Outstanding365Day" || dtEx.Columns[j].Caption.ToString() == "OutstandingUNAPPLIED_LASTDAY")
                                {
                                    if (dtEx.Columns[j].Caption.ToString() == "ClientCoName")
                                    {
                                        strBody.Append("<tr>");
                                    }
                                    strBody.Append("<td align='center'>" + dtEx.Rows[i][j].ToString() + "</td>");

                                    if (dtEx.Columns[j].Caption.ToString() == "OutstandingUNAPPLIED_LASTDAY")
                                    {
                                        strBody.Append("</tr>");
                                    }
                                }
                            }
                        }
                        strAttachment.Append("</tr>");

                    }

                    strAttachment.Append("</table>");

                    string filename = null;

                    filename = "CreditLimit_" + System.DateTime.Now.Year.ToString()
                        + "_" + System.DateTime.Now.Month.ToString()
                        + "_" + System.DateTime.Now.Day.ToString();

                    strBody.Append("</table>");
                    strBody.Append("<br/><br/>");

                    strBody.Append("<b><u>The below category of clients are marked in the attachment :</u></b><br/><br/>");
                    strBody.Append("Exception        : Clients, which are exempted from automated credit limit process.<br />");
                    strBody.Append("Category A       : Clients having More than credit Limit of Rs 3,00,000/-.<br />");
                    strBody.Append("Category B       : Clients having credit limit of Rs 3,00,000/- or less. These clients get ");
                    strBody.Append("blocked in case any invoice remains pending for payment beyond 120 days. System ");
                    strBody.Append("generated email goes to such clients in case they use their 70% of credit limit or any ");
                    strBody.Append("invoice remains pending for payment beyond 90 days.<br />");
                    strBody.Append("New Clients    : Clients, which are out of automated credit limit for initial 2 months ");
                    strBody.Append("from date of creation and such clients carry credit limit as per CAM for the interim ");
                    strBody.Append("period.<br /><br />");
                    strBody.Append("For any clarification, you may reach out to Mr. Tej Prakash (M : 9971399315, tej.prakash@carzonrent.com)<br/><br/>");
                    strBody.Append("<br/><br/>");

                    strBody.Append("Regards,<br/><br/>");
                    strBody.Append("Credit Control Team<br/><br/><br/><br/>");
                    strBody.Append("This is system generated mail. Please do not reply.");
                    strBody.Append("</body></html>");

                    sendMailNew(filename, strBody.ToString(), strAttachment.ToString(), "CreditControlEmail@carzonrent.com");
                }

                InstaConnection.Close();
            }
            catch (Exception ex)
            {
                LogErrorToLogFile(ex, "", "SendCreditLimitMailNew");
                InstaConnection.Close();
                //MessageBox.Show(ex.Message.ToString());
            }
        }

        private void sendMailNew(string FileName, string mailContent, string FileContent, string EmailID)
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                string From = "CreditControl@carzonrent.com";

                //message.To.Add("rahul.jain@carzonrent.com");
                message.To.Add("CreditControlEmail@carzonrent.com");
                message.Bcc.Add("techreports@carzonrent.com");

                message.Subject = "Credit Limit Exceeded & Contract Status";
                message.From = new System.Net.Mail.MailAddress(From);
                message.IsBodyHtml = true;
                byte[] excelFile = System.Text.Encoding.UTF8.GetBytes(FileContent.ToString());
                MemoryStream ms = new MemoryStream();
                ms.Write(excelFile, 0, excelFile.Length);
                ms.Position = 0;
                Attachment attachment = new Attachment(ms, FileName + ".xls", "application/vnd.xls");
                message.Attachments.Add(attachment);
                message.Priority = MailPriority.High;
                message.Body = mailContent.ToString();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("CreditControl@carzonrent.com", "Credit@1234");
                smtp.Send(message);
                Thread.Sleep(1000);
                //MessageBox.Show("Send");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                LogErrorToLogFile(ex, "", "sendMailNew");
            }
        }

        public void BlockUnblock()
        {
            SqlConnection InstaConnection = new SqlConnection(Instaconn);

            if (InstaConnection.State == ConnectionState.Closed)
            {
                InstaConnection.Open();
            }

            string UpdateBlockNUnblock = null;

            if (System.DateTime.Now.Hour == 9 || System.DateTime.Now.Hour == 10 || System.DateTime.Now.Hour == 12)
            {
                //Block and Unblock
                UpdateBlockNUnblock = "exec Prc_UpdateOutstanding_Block_N_UnBlock_New ";
            }
            if (System.DateTime.Now.Hour == 19)
            {
                //unblock
                UpdateBlockNUnblock = "exec Prc_UpdateOutstanding_UnBlock_New ";
            }

            SqlCommand cmdBlockNUnblock = new SqlCommand(UpdateBlockNUnblock, InstaConnection);
            cmdBlockNUnblock.ExecuteNonQuery();

            InstaConnection.Close();
        }
        //*****************************************************************************************


        //**************************************Float Coding Start*********************************
        /*
        public void DailyFloatMail()
        {
            SqlConnection InstaConnection = new SqlConnection(Instaconn);

            if (InstaConnection.State == ConnectionState.Closed)
            {
                InstaConnection.Open();
            }

            try
            {
                StringBuilder strAttachment = new StringBuilder();
                SqlCommand cmdSummary = new SqlCommand("exec prc_dailymailerFloat", InstaConnection); //added on 10-July-2015
                SqlDataAdapter daSummary = new SqlDataAdapter();
                daSummary.SelectCommand = cmdSummary;
                DataTable dtEx = new DataTable();
                daSummary.Fill(dtEx);

                string filename = null, EmailID = "";

                if (dtEx.Rows.Count > 0)
                {


                    StringBuilder strBody = new StringBuilder();

                    strBody.Append("<html xmlns='http://www.w3.org/1999/xhtml' >");
                    strBody.Append("Dear Sir / Madam,<br/><br/>");
                    strBody.Append("Greetings from Carzonrent……!");
                    strBody.Append("<br/><br/>");
                    strBody.Append("Please find below the float account details");
                    strBody.Append("<br/><br/>");
                    strBody.Append("<table border='1'>");

                    strBody.Append("<tr>");
                    strBody.Append("<td>Client Name</td>");
                    strBody.Append("<td>Relationship Manager</td>");
                    strBody.Append("<td>City Name</td>");
                    strBody.Append("<td>Credit Limit (INR)</td>");
                    strBody.Append("<td>Billing Current Month (Including Open Duties)</td>");
                    strBody.Append("<td>Billing (>30 Days)</td>");
                    strBody.Append("<td>Status</td>");
                    strBody.Append("<td>Category</td>");


                    strBody.Append("</tr>");


                    //strBody = "";
                    string Subject = string.Empty;
                    Subject = "Credit Limit – Float Account";
                    string CCEmailID = "";

                    for (int i = 0; i < dtEx.Rows.Count; i++)
                    {
                        if (EmailID == "")
                        {
                            EmailID = dtEx.Rows[i]["relationmanagerid"].ToString().Trim();
                            CCEmailID = dtEx.Rows[i]["ccemail"].ToString().Trim();
                        }
                        else
                        {
                            if (!CCEmailID.Contains(dtEx.Rows[i]["relationmanagerid"].ToString()))
                            {
                                EmailID = EmailID + "," + dtEx.Rows[i]["relationmanagerid"].ToString().Trim();
                            }
                            if (!CCEmailID.Contains(dtEx.Rows[i]["ccemail"].ToString()))
                            {
                                CCEmailID = CCEmailID + "," + dtEx.Rows[i]["ccemail"].ToString().Trim();
                            }
                        }


                        strBody.Append("<tr>");
                        strBody.Append("<td>" + dtEx.Rows[i]["clientconame"].ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["RelationshipManagername"].ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["CityName"].ToString() + "</td>");
                        //strBody.Append("<td>" + (Convert.ToDouble(dtEx.Rows[i]["CreditLimit"]) - Convert.ToDouble(dtEx.Rows[i]["CurrentMonthBilling"].ToString())).ToString() + "</td>");
                        strBody.Append("<td>" + (Convert.ToDouble(dtEx.Rows[i]["CreditLimit"])).ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["CurrentMonthBilling"].ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["BillingMoreThan30"].ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["Active"].ToString() + "</td>");
                        strBody.Append("<td>" + dtEx.Rows[i]["credittype"].ToString() + "</td>");
                        strBody.Append("</tr>");



                    }

                    strBody.Append("</table>");
                    strBody.Append("<br/><br/>");
                    strBody.Append("With Regards,");
                    strBody.Append("<br/>");
                    strBody.Append("Credit Control Team");
                    strBody.Append("<br/>");
                    strBody.Append("Carzonrent");
                    strBody.Append("<br/><br/><br/><br/>");
                    strBody.Append("This is system generated mail. Please do not reply.");

                    sendFloatMail(filename, strBody.ToString(), strAttachment.ToString(), EmailID, Subject, CCEmailID);
                }
                InstaConnection.Close();
            }
            catch (Exception ex)
            {
                LogErrorToLogFile(ex, "", "DailyFloatMail");
                InstaConnection.Close();
                //MessageBox.Show(ex.Message.ToString());
            }
        }
        */

        public static string LogErrorToLogFile(Exception ee, string userFriendlyError, string ModuleName)
        {
            try
            {
                string path = @"D:\ErrorLog\";
                // check if directory exists
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path = path + ModuleName + DateTime.Today.ToString("dd-MMM-yy") + ".log";
                // check if file exist
                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                }
                // log the error now
                using (StreamWriter writer = File.AppendText(path))
                {
                    string error = "\r\nLog written at : " + DateTime.Now.ToString() +
                    "\r\n\r\nHere is the actual error :\n" + ee.ToString();
                    writer.WriteLine(error);
                    writer.WriteLine("==========================================");
                    writer.Flush();
                    writer.Close();
                }
                return userFriendlyError;
            }
            catch (Exception ex)
            {
                //throw;
                return ex.ToString();
            }
        }
        /*
        private void sendFloatMail(string FileName, string mailContent, string FileContent
           , string EmailID, string Subject, string CCEmailID)
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                string From = "CreditControl@carzonrent.com";
                message.To.Add(EmailID);
                message.CC.Add(CCEmailID);
                message.Bcc.Add("techreports@carzonrent.com");

                message.Subject = Subject;
                message.From = new System.Net.Mail.MailAddress(From);
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;
                message.Body = mailContent.ToString();
                message.ReplyTo = new System.Net.Mail.MailAddress("creditcor@carzonrent.com");
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("CreditControl@carzonrent.com", "Credit@1234");
                smtp.Send(message);
                Thread.Sleep(1000);
                //MessageBox.Show("Send");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                //if (EmailID != "ajay.mishra@carzonrent.com")
                //{
                //    sendMail(FileName, mailContent, FileContent
                //    , "ajay.mishra@carzonrent.com", "Delivery Failure for emailid : " + EmailID);
                //}
            }
        }
        //**************************************Float Coding End***********************************
        */

        public void UpdateInvoiceDetails()
        {
            try
            {
                OracleConnection Oraconnect = new OracleConnection(oracleconn);
                Oraconnect.Open();

                SqlConnection InstaConnection = new SqlConnection(Instaconn);
                InstaConnection.Open();

                //SqlCommand getInstadetail = new SqlCommand("select * from vOracledetails where cast(accountingdate as date) between '2014-04-01' and '2014-04-30' ", InstaConnection);
                SqlCommand getInstadetail = new SqlCommand("exec prcOracledetails '2014-04-01' ,'2014-07-31' ", InstaConnection);
                getInstadetail.CommandTimeout = 18000;
                SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
                DataSet dsInstadetail = new DataSet();
                dbInstadetail.Fill(dsInstadetail);

                if (dsInstadetail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsInstadetail.Tables[0].Rows.Count; i++)
                    {
                        double OracleOutstanding = 0;

                        OracleCommand objCmd = new OracleCommand();
                        objCmd.Connection = Oraconnect;
                        objCmd.CommandText = "TRX_ATTRIB_UPDATE";
                        objCmd.CommandType = CommandType.StoredProcedure;
                        objCmd.Parameters.Add("P_TRX_NUMBER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["InvoiceNo"].ToString();
                        objCmd.Parameters.Add("P_REG_NUMBER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CarNO"].ToString();
                        objCmd.Parameters.Add("P_CLIENT", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CompanyName"].ToString();
                        objCmd.Parameters.Add("P_PICKUP_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["pickupdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_DROP_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["dropoffdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_GL_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["accountingdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_DS_NUM", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["DutySlipNo"].ToString();

                        try
                        {
                            int str = objCmd.ExecuteNonQuery();
                            //MessageBox.Show(str.ToString());

                            if (str == 1)
                            {
                                //update outstanding in table start
                                string UpdateOutstanding = "exec Prc_UpdateOracleStatus " + dsInstadetail.Tables[0].Rows[i]["BookingID"].ToString() + ",1"; ;
                                SqlCommand cmdupdateOutstanding = new SqlCommand(UpdateOutstanding, InstaConnection);
                                cmdupdateOutstanding.ExecuteNonQuery();
                                //update outstanding in table end
                            }
                        }
                        catch (Exception ex)
                        {
                            //MessageBox.Show(ex.Message.ToString());
                            //update outstanding in table start
                            //string UpdateOutstanding1 = "exec Prc_UpdateOracleStatus " + dsInstadetail.Tables[0].Rows[i]["BookingID"].ToString() + ",1"; ;
                            //SqlCommand cmdupdateOutstanding1 = new SqlCommand(UpdateOutstanding1, InstaConnection);
                            //cmdupdateOutstanding1.ExecuteNonQuery();
                            //update outstanding in table end
                        }
                    }
                }
                else
                {
                    Application.Exit();
                }

                Oraconnect.Close();
                InstaConnection.Close();
            }
            catch (Exception Ex)
            {
                //MessageBox.Show(Ex.Message.ToString());
            }
        }


        public void InsertInvoiceDetails()
        {
            try
            {
                OracleConnection Oraconnect = new OracleConnection(oracleconn);
                Oraconnect.Open();

                SqlConnection InstaConnection = new SqlConnection(Instaconn);
                InstaConnection.Open();

                SqlCommand getInstadetail = new SqlCommand("exec prcOracledetails_Insert", InstaConnection);
                getInstadetail.CommandTimeout = 18000;
                SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
                DataSet dsInstadetail = new DataSet();
                dbInstadetail.Fill(dsInstadetail);

                if (dsInstadetail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsInstadetail.Tables[0].Rows.Count; i++)
                    {
                        OracleCommand objCmd = new OracleCommand();
                        objCmd.Connection = Oraconnect;
                        objCmd.CommandText = "TRX_DFF_INSERT";
                        objCmd.CommandType = CommandType.StoredProcedure;
                        objCmd.Parameters.Add("P_PRODUCT", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["Product"].ToString();
                        objCmd.Parameters.Add("P_TRX_NUMBER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["InvoiceNo"].ToString();
                        objCmd.Parameters.Add("P_TRX_DATE", OracleType.VarChar).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["accountingdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_CREATION_DATE", OracleType.VarChar).Value = Convert.ToDateTime(System.DateTime.Now).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_PICKUP_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["pickupdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_DROP_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["dropoffdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_GL_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["accountingdate"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_DS_NUMBER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["DutySlipNo"].ToString();

                        objCmd.Parameters.Add("P_BEGIN_KM", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["kmout"].ToString();
                        objCmd.Parameters.Add("P_END_KM", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["Kmin"].ToString();
                        objCmd.Parameters.Add("P_PACKAGE_NAME", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["pkgdetail"].ToString();
                        objCmd.Parameters.Add("P_CUSTOMER_NAME", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CompanyName"].ToString();
                        objCmd.Parameters.Add("P_VENDOR_NAME", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["Vendorname"].ToString();
                        objCmd.Parameters.Add("P_CHAUFFER_NAME", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["ChauffeurName"].ToString();
                        objCmd.Parameters.Add("P_USER_NAME", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["GuestName"].ToString();
                        objCmd.Parameters.Add("P_CAR_CATEGORY", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CategoryName"].ToString();
                        objCmd.Parameters.Add("P_CAR_MODEL", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["ModelName"].ToString();
                        objCmd.Parameters.Add("P_REGN_NO", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CarNO"].ToString();
                        objCmd.Parameters.Add("P_PICKUP_LOCATION", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CityName"].ToString();
                        objCmd.Parameters.Add("P_SOURCE", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["SOURCE"].ToString(); //to work on it
                        objCmd.Parameters.Add("P_REVENUE_AMOUNT", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["basic"].ToString();
                        objCmd.Parameters.Add("P_AIRPORT_PARKING", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["AirportParking"].ToString(); //to work on it
                        objCmd.Parameters.Add("P_CALL_CENTER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CallCenter"].ToString(); //to work on it
                        objCmd.Parameters.Add("P_NCR_ENTRY", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["NCREntry"].ToString(); //to work on it
                        objCmd.Parameters.Add("P_REVENUE_PARKING", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["Parking"].ToString(); //old name P_PARKING_AMOUNT

                        objCmd.Parameters.Add("P_STAX_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["servicetaxpercent"]);
                        objCmd.Parameters.Add("P_STAX_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["servicetaxamt"]);
                        objCmd.Parameters.Add("P_KRISHI_KALYAN_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["KrishiKalyantaxpercent"]);
                        objCmd.Parameters.Add("P_KRISHI_KALYAN_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["KrishiKalyanTaxAmt"]);
                        objCmd.Parameters.Add("P_SWACHH_BHARAT_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["SwachhBharattaxpercent"]);
                        objCmd.Parameters.Add("P_SWACHH_BHARAT_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["SwachhBharatTaxAmt"]);

                        objCmd.Parameters.Add("P_TAX_AMOUNT", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["taxes"].ToString();

                        objCmd.Parameters.Add("P_INVOICE_AMOUNT", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["totalcost"].ToString();
                        objCmd.Parameters.Add("P_STATUS_TRX", OracleType.VarChar).Value = Convert.ToInt32(dsInstadetail.Tables[0].Rows[i]["STATUS_TRX"]);
                        objCmd.Parameters.Add("P_COR_TAX", OracleType.VarChar).Value = Convert.ToInt32(dsInstadetail.Tables[0].Rows[i]["CorTax"]);

                        objCmd.Parameters.Add("P_CUSTOMER_GSTIN", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["CUSTOMER_GSTIN"]);
                        objCmd.Parameters.Add("P_REG_STATUS", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["REG_STATUS"]);
                        objCmd.Parameters.Add("P_LOCATION", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["LOCATION"]);
                        objCmd.Parameters.Add("P_STATE", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["STATE"]);

                        objCmd.Parameters.Add("P_SGST_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["SGST_RATE"]);
                        objCmd.Parameters.Add("P_SGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["SGST_AMOUNT"]);
                        objCmd.Parameters.Add("P_CGST_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["CGST_RATE"]);
                        objCmd.Parameters.Add("P_CGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["CGST_AMOUNT"]);
                        objCmd.Parameters.Add("P_IGST_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["IGST_RATE"]);
                        objCmd.Parameters.Add("P_IGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["IGST_AMOUNT"]);

                        //change
                        objCmd.Parameters.Add("P_BATCH_TRX_NUMBER", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_TRX_NUMBER"]);
                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_TRX_DATE"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_TRX_DATE", OracleType.DateTime).Value = Convert.ToDateTime("1900-01-01").ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_TRX_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["BATCH_TRX_DATE"]);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_GL_DATE"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_GL_DATE", OracleType.DateTime).Value = Convert.ToDateTime("1900-01-01").ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_GL_DATE", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["BATCH_GL_DATE"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_PERIOD_START"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_PERIOD_START", OracleType.DateTime).Value = Convert.ToDateTime("1900-01-01").ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_PERIOD_START", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["BATCH_PERIOD_START"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_PERIOD_END"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_PERIOD_END", OracleType.DateTime).Value = Convert.ToDateTime("1900-01-01").ToString("dd-MMM-yyyy");
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_PERIOD_END", OracleType.DateTime).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["BATCH_PERIOD_END"]);
                        }


                        objCmd.Parameters.Add("P_BATCH_CUSTOMER_NAME", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_CUSTOMER_NAME"]);
                        objCmd.Parameters.Add("P_BATCH_REG_STATUS", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_REG_STATUS"]);
                        objCmd.Parameters.Add("P_BATCH_CUSTOMER_GSTIN", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_CUSTOMER_GSTIN"]);
                        objCmd.Parameters.Add("P_CUSTOMER_SUBSIDIARY_NAME", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["CUSTOMER_SUBSIDIARY_NAME"]);
                        objCmd.Parameters.Add("P_BATCH_EXEMPTION_FLAG", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_EXEMPTION_FLAG"]);
                        objCmd.Parameters.Add("P_EXEMPTION_FLAG", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["EXEMPTION_FLAG"]);
                        objCmd.Parameters.Add("P_BATCH_LOCATION", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_LOCATION"]);
                        objCmd.Parameters.Add("P_BATCH_GST_STATE", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_GST_STATE"]);

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_REVENUE_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_REVENUE_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_REVENUE_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_REVENUE_AMOUNT"]);
                        }


                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_PARKING_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_PARKING_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_PARKING_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_PARKING_AMOUNT"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_GST_RATE"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_GST_RATE", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_GST_RATE", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_GST_RATE"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_CGST_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_CGST_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_CGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_CGST_AMOUNT"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_SGST_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_SGST_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_SGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_SGST_AMOUNT"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_IGST_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_IGST_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_IGST_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_IGST_AMOUNT"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_INVOICE_AMOUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_INVOICE_AMOUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_INVOICE_AMOUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_INVOICE_AMOUNT"]);
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["BATCH_INVOICE_COUNT"])))
                        {
                            objCmd.Parameters.Add("P_BATCH_INVOICE_COUNT", OracleType.Number).Value = "0";
                        }
                        else
                        {
                            objCmd.Parameters.Add("P_BATCH_INVOICE_COUNT", OracleType.Number).Value = Convert.ToDouble(dsInstadetail.Tables[0].Rows[i]["BATCH_INVOICE_COUNT"]);
                        }

                        objCmd.Parameters.Add("P_SEZ_FLAG", OracleType.Number).Value = Convert.ToInt32(dsInstadetail.Tables[0].Rows[i]["SEZClientYN"]);


                        objCmd.Parameters.Add("P_CATEGORY", OracleType.VarChar).Value = Convert.ToString(dsInstadetail.Tables[0].Rows[i]["ServiceType"]);


                        try
                        {

                            int str = objCmd.ExecuteNonQuery();
                            objCmd.Dispose();
                            //MessageBox.Show(str.ToString());

                            if (str == 1)
                            {
                                //update outstanding in table start
                                string UpdateOutstanding = "exec Prc_UpdateOracleStatus " + dsInstadetail.Tables[0].Rows[i]["BookingID"].ToString() + ",0," + dsInstadetail.Tables[0].Rows[i]["ID"].ToString();
                                SqlCommand cmdupdateOutstanding = new SqlCommand(UpdateOutstanding, InstaConnection);
                                cmdupdateOutstanding.ExecuteNonQuery();
                                cmdupdateOutstanding.Dispose();
                                //update outstanding in table end
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.ToString().Contains("ORA-00001"))
                            {
                                string UpdateOutstanding1 = "exec Prc_UpdateOracleStatus " + dsInstadetail.Tables[0].Rows[i]["BookingID"].ToString() + ",2," + dsInstadetail.Tables[0].Rows[i]["ID"].ToString();
                                SqlCommand cmdupdateOutstanding1 = new SqlCommand(UpdateOutstanding1, InstaConnection);
                                cmdupdateOutstanding1.ExecuteNonQuery();
                                cmdupdateOutstanding1.Dispose();
                            }
                            //MessageBox.Show(ex.Message.ToString());
                        }
                    }
                }

                Oraconnect.Close();
                InstaConnection.Close();
                getInstadetail.Dispose();

            }
            catch (Exception Ex)
            {
                //MessageBox.Show(Ex.Message.ToString());
            }
        }

        private void UpdateOraTable()
        {
            SqlConnection InstaCon = new SqlConnection(Instaconn);
            OracleConnection Oraconnect = new OracleConnection(oracleconn);

            DateTime StartDate = System.DateTime.Now;
            TimeSpan TimeDifference;

            string Message = string.Empty;

            if (InstaCon.State == ConnectionState.Closed)
            {
                InstaCon.Open();
            }

            //string sqlBatchDetail = "select * from vGetBTCBatchNO "; //code commented on 18-Nov-2014
            //SqlCommand cmdGetBatchDetail = new SqlCommand(sqlBatchDetail, InstaCon); //code commented on 18-Nov-2014

            SqlCommand cmdGetBatchDetail = new SqlCommand("Prc_GetBTCBatchNO", InstaCon); //new code on 18-Nov-2014
            cmdGetBatchDetail.CommandType = CommandType.StoredProcedure; //new code on 18-Nov-2014

            cmdGetBatchDetail.CommandTimeout = 18000;
            SqlDataAdapter sqlAdapterBatch = new SqlDataAdapter(cmdGetBatchDetail);
            DataSet sqlDatasetBatch = new DataSet();
            sqlAdapterBatch.Fill(sqlDatasetBatch);

            DateTime InstaDate = System.DateTime.Now;
            TimeDifference = InstaDate - StartDate;

            Message = "Insta View Time : " + TimeDifference.Minutes.ToString() + " Minutes and "
                + TimeDifference.Seconds.ToString() + " Seconds " + ", ";

            if (Oraconnect.State == ConnectionState.Closed)
            {
                Oraconnect.Open();
            }

            string delcomm;
            delcomm = " truncate table AR_BTC_BATCH ";
            OracleCommand oracledelcmd = new OracleCommand(delcomm, Oraconnect);
            //oracledelcmd.CommandTimeout = 18000;
            oracledelcmd.ExecuteNonQuery();

            DateTime DeleteDate = System.DateTime.Now;
            TimeDifference = DeleteDate - InstaDate;
            Message = Message + "Oracle delete Query Time : " + TimeDifference.Minutes.ToString() + " Minutes and "
                + TimeDifference.Seconds.ToString() + " Seconds " + ", ";


            OracleCommand oracleInsert = new OracleCommand();
            string submissiondate = string.Empty;
            string BatchCreatedate = string.Empty;

            for (int a = 0; a < sqlDatasetBatch.Tables[0].Rows.Count; a++)
            {
                try
                {
                    if (sqlDatasetBatch.Tables[0].Rows[a][2].ToString() == "")
                    {
                        submissiondate = sqlDatasetBatch.Tables[0].Rows[a][2].ToString();
                    }
                    else
                    {
                        submissiondate = Convert.ToDateTime(sqlDatasetBatch.Tables[0].Rows[a][2]).ToString("dd-MMM-yyyy");
                    }
                    BatchCreatedate = Convert.ToDateTime(sqlDatasetBatch.Tables[0].Rows[a][6]).ToString("dd-MMM-yyyy");

                    //Updateing Batch details in the oracle table
                    oracleInsert = new OracleCommand("AR_BTC_BATCH_INSERT", Oraconnect);
                    oracleInsert.CommandType = CommandType.StoredProcedure;

                    OracleParameter parm = new OracleParameter("p_booking_id", SqlDbType.NChar);
                    parm.Value = sqlDatasetBatch.Tables[0].Rows[a][0].ToString(); //Bookingid
                    parm.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm);

                    OracleParameter parm1 = new OracleParameter("p_batch_no", SqlDbType.NChar);
                    parm1.Value = sqlDatasetBatch.Tables[0].Rows[a][1].ToString(); //batchno
                    parm1.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm1);

                    OracleParameter parm2 = new OracleParameter("p_submission_date", SqlDbType.NChar);
                    parm2.Value = submissiondate; //submission date
                    parm2.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm2);

                    OracleParameter parm3 = new OracleParameter("p_submitted_to", SqlDbType.NChar);
                    parm3.Value = sqlDatasetBatch.Tables[0].Rows[a][3].ToString(); //submitted to 
                    parm3.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm3);

                    OracleParameter parm4 = new OracleParameter("p_submitted_location", SqlDbType.NChar);
                    parm4.Value = sqlDatasetBatch.Tables[0].Rows[a][4].ToString();
                    parm4.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm4);

                    OracleParameter parm5 = new OracleParameter("p_awbno", SqlDbType.NChar);
                    parm5.Value = sqlDatasetBatch.Tables[0].Rows[a][5].ToString();
                    parm5.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm5);

                    OracleParameter parm6 = new OracleParameter("p_batchcreated_date", SqlDbType.NChar);
                    parm6.Value = BatchCreatedate;
                    parm6.Direction = ParameterDirection.Input;
                    oracleInsert.Parameters.Add(parm6);

                    oracleInsert.CommandTimeout = 18000;
                    oracleInsert.ExecuteNonQuery();

                    //if (a == 100000 || a == 200000 || a == 300000 || a == 400000)
                    //{
                    //    //MessageBox.Show(a.ToString());
                    //}

                    //insertcmd = new StringBuilder(" insert into AR_BTC_BATCH (\"BookingID\", \"BatchNO\"");
                    //insertcmd.Append(" , \"SubmissionDate\", \"SubmittedTo\", \"SubmittedLocation\", \"AWBno\", \"BatchCreateDate\") ");
                    //insertcmd.Append(" values ( '" + sqlDatasetBatch.Tables[0].Rows[a][0].ToString() + "', ");
                    //insertcmd.Append(" '" + sqlDatasetBatch.Tables[0].Rows[a][1].ToString() + "', ");
                    //if (sqlDatasetBatch.Tables[0].Rows[a][2].ToString() == "")
                    //{
                    //    insertcmd.Append(" '" + sqlDatasetBatch.Tables[0].Rows[a][2].ToString() + "', ");
                    //}
                    //else
                    //{
                    //    insertcmd.Append(" to_date('" + Convert.ToDateTime(sqlDatasetBatch.Tables[0].Rows[a][2]).ToShortDateString() + "','MM/DD/YYYY'), ");
                    //}
                    //insertcmd.Append(" '" + sqlDatasetBatch.Tables[0].Rows[a][3].ToString() + "', ");
                    //insertcmd.Append(" '" + sqlDatasetBatch.Tables[0].Rows[a][4].ToString() + "', ");
                    //insertcmd.Append(" '" + sqlDatasetBatch.Tables[0].Rows[a][5].ToString() + "', ");
                    //insertcmd.Append(" to_date('" + Convert.ToDateTime(sqlDatasetBatch.Tables[0].Rows[a][6]).ToShortDateString() + "','MM/DD/YYYY')) ");

                    //OracleCommand insertoracmd = new OracleCommand(insertcmd.ToString(), Oraconnect);
                    //insertoracmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);

                }
            }

            DateTime InsertDate = System.DateTime.Now;
            TimeDifference = InsertDate - DeleteDate;
            Message = Message + "Oracle Insert Query Time : " + TimeDifference.Hours.ToString() + " Hours and "
                + TimeDifference.Minutes.ToString() + " Minutes and "
                + TimeDifference.Seconds.ToString() + " Seconds " + ".";

            //MessageBox.Show(Message);
            //Close();
        }

        public void UpdateOracleCarCode()
        {
            OracleConnection Oraconnect = new OracleConnection(oracleconn);
            Oraconnect.Open();

            SqlConnection InstaConnection = new SqlConnection(Instaconn);
            InstaConnection.Open();

            string ViewUpdate = "DBMS_APPLICATION_INFO.SET_CLIENT_INFO(82)";
            OracleCommand cmdStart = new OracleCommand(ViewUpdate, Oraconnect);
            cmdStart.CommandType = CommandType.StoredProcedure;
            cmdStart.ExecuteNonQuery();

            SqlCommand getInstadetail = InstaConnection.CreateCommand();
            getInstadetail.CommandText = "select RegnNo, OracleCarCode, vendorCarID  FROM CORIntVendorCarMaster where Active = 1 and ApproveYN = 1 and OracleCarCode is null ";

            getInstadetail.CommandTimeout = 18000;
            SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
            DataSet dsInstadetail = new DataSet();

            dbInstadetail.Fill(dsInstadetail);
            if (dsInstadetail.Tables[0].Rows.Count > 0)
            {
                int OracleCarCode = 0;
                StringBuilder OraUpdateCarCode = new StringBuilder();
                for (int i = 0; i <= dsInstadetail.Tables[0].Rows.Count - 1; i++)
                {
                    OracleCarCode = GetOracleCarCode(dsInstadetail.Tables[0].Rows[i]["RegnNo"].ToString(), Oraconnect);

                    if (OracleCarCode > 0)
                    {
                        SqlCommand command = InstaConnection.CreateCommand();
                        command.CommandText = "UPDATE CORIntVendorCarMaster SET OracleCarCode = @OracleCarCode where vendorCarID = @vendorCarID";
                        command.Parameters.AddWithValue("OracleCarCode", OracleCarCode);
                        command.Parameters.AddWithValue("vendorCarID", dsInstadetail.Tables[0].Rows[i]["vendorCarID"].ToString());
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public int GetOracleCarCode(string RegnNo, OracleConnection Oraconnect)
        {
            int OracleCarCode = 0;
            try
            {
                if (Oraconnect.State == ConnectionState.Closed)
                {
                    Oraconnect.Open();
                }

                StringBuilder OraUpdateCarCode = new StringBuilder("select car_code('" + RegnNo + "') CarCode from dual");
                OracleCommand searchoracleCarCode = new OracleCommand(OraUpdateCarCode.ToString(), Oraconnect);
                OracleDataAdapter oraAdapter = new OracleDataAdapter(searchoracleCarCode);

                DataSet OraGetCarCode = new DataSet();

                oraAdapter.Fill(OraGetCarCode);
                if (OraGetCarCode.Tables[0].Rows.Count > 0)
                {
                    for (int k = 0; k < OraGetCarCode.Tables[0].Rows.Count; k++)
                    {
                        if (OraGetCarCode.Tables[0].Rows[k]["CarCode"].ToString() != "")
                        {
                            OracleCarCode = Convert.ToInt32(OraGetCarCode.Tables[0].Rows[k]["CarCode"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return OracleCarCode;
        }

        public void InsertCorrectGSTN()
        {
            try
            {
                OracleConnection Oraconnect = new OracleConnection(oracleconn);
                Oraconnect.Open();

                SqlConnection InstaConnection = new SqlConnection(Instaconn);
                InstaConnection.Open();

                SqlCommand getInstadetail = new SqlCommand("exec Prc_CorrectGstnNo", InstaConnection);
                getInstadetail.CommandTimeout = 18000;
                SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
                DataSet dsInstadetail = new DataSet();
                dbInstadetail.Fill(dsInstadetail);

                if (dsInstadetail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsInstadetail.Tables[0].Rows.Count; i++)
                    {
                        double OracleOutstanding = 0;

                        OracleCommand objCmd = new OracleCommand();
                        objCmd.Connection = Oraconnect;
                        objCmd.CommandText = "TRX_GST_INSERT";
                        objCmd.CommandType = CommandType.StoredProcedure;
                        objCmd.Parameters.Add("P_TRX_NUMBER", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["TRX_NUMBER"].ToString();
                        objCmd.Parameters.Add("P_TRX_DATE", OracleType.VarChar).Value = Convert.ToDateTime(dsInstadetail.Tables[0].Rows[i]["TRX_DATE"]).ToString("dd-MMM-yyyy");
                        objCmd.Parameters.Add("P_REG_STATUS", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["REG_STATUS"].ToString();
                        objCmd.Parameters.Add("P_CUSTOMER_GSTIN", OracleType.VarChar).Value = dsInstadetail.Tables[0].Rows[i]["CUSTOMER_GSTIN"].ToString();

                        try
                        {
                            int str = objCmd.ExecuteNonQuery();
                            objCmd.Dispose();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                Oraconnect.Close();
                InstaConnection.Close();
                getInstadetail.Dispose();

            }
            catch (Exception Ex)
            {
                //MessageBox.Show(Ex.Message.ToString());
            }
        }

        public void UpdateOracleClientNameInInsta()
        {
            try
            {
                OracleConnection Oraconnect = new OracleConnection(oracleconn);
                Oraconnect.Open();

                SqlConnection InstaConnection = new SqlConnection(Instaconn);
                InstaConnection.Open();

                SqlCommand getInstadetail = new SqlCommand("select ClientCoID, ClientCoName from corintclientcomaster  where OracleClientName is null ", InstaConnection);
                getInstadetail.CommandTimeout = 18000;
                SqlDataAdapter dbInstadetail = new SqlDataAdapter(getInstadetail);
                DataSet dsInstadetail = new DataSet();
                dbInstadetail.Fill(dsInstadetail);

                if (dsInstadetail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsInstadetail.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            double OracleOutstanding = 0;

                            StringBuilder OraOutstanding = new StringBuilder(" select CUSTOMER_NAME_INSTA, CUSTOMER_NAME_ORACLE ");
                            OraOutstanding.Append(", CUSTOMER_CODE from CS_MAPPING where CUSTOMER_NAME_INSTA='");
                            OraOutstanding.Append(Convert.ToString(dsInstadetail.Tables[0].Rows[i]["ClientCoName"]) + "' and ROWNUM = 1 ");
                            OracleCommand searchoracle = new OracleCommand(OraOutstanding.ToString(), Oraconnect);
                            OracleDataAdapter oraAdapter = new OracleDataAdapter(searchoracle);

                            DataSet OraDataset = new DataSet();

                            oraAdapter.Fill(OraDataset);


                            if (OraDataset.Tables[0].Rows.Count > 0)
                            {
                                //update oracle client name in insta start
                                string UpdateOracleClientName = "update corintclientcomaster set OracleClientName = '"
                                    + Convert.ToString(OraDataset.Tables[0].Rows[0]["CUSTOMER_NAME_ORACLE"]) + "'"
                                    + " where clientcoid = "
                                    + Convert.ToInt32(dsInstadetail.Tables[0].Rows[i]["ClientCoID"]);
                                SqlCommand cmdupdateOracleClientName = new SqlCommand(UpdateOracleClientName, InstaConnection);
                                cmdupdateOracleClientName.ExecuteNonQuery();
                                //update oracle client name in insta end
                            }

                            searchoracle.Dispose();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                Oraconnect.Close();
                InstaConnection.Close();
                getInstadetail.Dispose();

            }
            catch (Exception Ex)
            {
                //MessageBox.Show(Ex.Message.ToString());
            }
        }



        public void GetOracleTotalcost()
        {
            OracleConnection Oraconnect = new OracleConnection(oracleconn);
            Oraconnect.Open();

            SqlConnection InstaConnection = new SqlConnection(Instaconn);
            InstaConnection.Open();

            string ViewUpdate = "DBMS_APPLICATION_INFO.SET_CLIENT_INFO(82)";
            OracleCommand cmdStart = new OracleCommand(ViewUpdate, Oraconnect);
            cmdStart.CommandType = CommandType.StoredProcedure;
            cmdStart.ExecuteNonQuery();

            StringBuilder OraUpdateCarCode = new StringBuilder("select * from AR_PAYMENT_SCHEDULES_ALL where TRX_NUMBER  ='KA/1920/C/069093'");
            //StringBuilder OraUpdateCarCode = new StringBuilder("select AMOUNT_DUE_ORIGINAL from AR_PAYMENT_SCHEDULES_ALL where TRX_NUMBER  in ('KA/1819/C/057964','KA/1819/C/057994'");
            OracleCommand searchoracleCarCode = new OracleCommand(OraUpdateCarCode.ToString(), Oraconnect);
            OracleDataAdapter oraAdapter = new OracleDataAdapter(searchoracleCarCode);

            DataSet OraGetCarCode = new DataSet();

            oraAdapter.Fill(OraGetCarCode);
            MessageBox.Show(OraGetCarCode.Tables[0].Rows[0]["AMOUNT_DUE_ORIGINAL"].ToString());
        }

        // Start CarHire 

        private void UpdateVendorOracleCode()
        {
            try
            {

                OracleConnection Oraconnect = null;
                SqlConnection InstaConnection = null;
                string oracleVendorName = string.Empty;
                int lastIndex = 0;

                using (InstaConnection = new SqlConnection(Instaconn))
                {
                    InstaConnection.Open();
                    SqlCommand getVendorDetails = new SqlCommand("exec Prc_GetVendorDetailsForOracleCode", InstaConnection);
                    getVendorDetails.CommandTimeout = 18000;
                    SqlDataAdapter adpInstadetail = new SqlDataAdapter(getVendorDetails);
                    DataSet dsVendor = new DataSet();
                    DataSet dsOrlVendor = new DataSet();
                    adpInstadetail.Fill(dsVendor);
                    if (dsVendor.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsVendor.Tables[0].Rows)
                        {
                            if (Convert.ToString(dr["CarVendorName"]).EndsWith("_") || Convert.ToString(dr["CarVendorName"]).EndsWith("-"))
                            {
                                if (Convert.ToString(dr["CarVendorName"]).EndsWith("_"))
                                {
                                    lastIndex = Convert.ToString(dr["CarVendorName"]).LastIndexOf("_");
                                }
                                else
                                {
                                    lastIndex = Convert.ToString(dr["CarVendorName"]).LastIndexOf("-");
                                }
                                oracleVendorName = Convert.ToString(dr["CarVendorName"]).Substring(0, lastIndex - 1) + " " + "_" + " " + Convert.ToString(dr["PANNo"]);
                            }
                            else
                            {
                                oracleVendorName = Convert.ToString(dr["CarVendorName"]) + " " + "_" + " " + Convert.ToString(dr["PANNo"]);
                            }
                            if (!string.IsNullOrEmpty(oracleVendorName))
                            {
                                using (Oraconnect = new OracleConnection(oracleconn))
                                {
                                    Oraconnect.Open();
                                    OracleCommand objCmd = new OracleCommand();
                                    objCmd.Connection = Oraconnect;
                                    objCmd.CommandText = "TRX_DFF_INSERT";
                                    objCmd.CommandType = CommandType.StoredProcedure;
                                    objCmd.Parameters.AddWithValue("V_Name", oracleVendorName);
                                    OracleDataAdapter oralAdp = new OracleDataAdapter(objCmd);
                                    oralAdp.Fill(dsOrlVendor);
                                    if (dsOrlVendor.Tables[0].Rows.Count > 0)
                                    {
                                        SqlCommand upVendorDetails = new SqlCommand("exec Prc_UpdateOracleCodeVendor", InstaConnection);
                                        upVendorDetails.Parameters.AddWithValue("@CarVendorId", Convert.ToInt32(dr["CarVendorID"]));
                                        upVendorDetails.Parameters.AddWithValue("@OracleCode", dsOrlVendor.Tables[0].Rows[0]["OracleCode"]);
                                        upVendorDetails.ExecuteNonQuery();
                                    }

                                }
                            }
                        }
                        //oracleVendorName=
                    }
                }

            }

            catch (Exception Ex)
            {
                throw;
            }

        }
        /*  discussion with oracle team.
        private void UpdateVendorSiteName()
        {
            DataSet dsOrlVendor = new DataSet();
            OracleConnection Oraconnect = new OracleConnection();
            try
            {
                using (Oraconnect = new OracleConnection(oracleconn))
                {
                    Oraconnect.Open();
                    OracleCommand objCmd = new OracleCommand();
                    objCmd.Connection = Oraconnect;
                    objCmd.CommandText = "TRX_DFF_INSERT";
                    objCmd.CommandType = CommandType.StoredProcedure;
                    objCmd.Parameters.AddWithValue("V_Name", oracleVendorName);
                    OracleDataAdapter oralAdp = new OracleDataAdapter(objCmd);
                    oralAdp.Fill(dsOrlVendor);
                    if (dsOrlVendor.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsOrlVendor.Tables[0].Rows)
                        {
                            SqlCommand upVendorDetails = new SqlCommand("exec InsertVendorSiteDetails", InstaConnection);
                            upVendorDetails.Parameters.AddWithValue("@VendorOracleCode", Convert.ToInt32(dr["OracleCode"]));
                            upVendorDetails.Parameters.AddWithValue("@Vendor_Oracle_SiteID", dsOrlVendor.Tables[0].Rows[0]["SiteID"]);
                            upVendorDetails.Parameters.AddWithValue("@Vendor_Site_Code", dsOrlVendor.Tables[0].Rows[0]["SiteCode"]);
                            upVendorDetails.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        */
        private void GetOracleVendorNameAndOracleCode()
        {
            try
            {

                OracleConnection Oraconnect = null;
                SqlConnection InstaConnection = null;
                string oracleVendorName = string.Empty;
                int lastIndex = 0;

                using (InstaConnection = new SqlConnection(Instaconn))
                {
                    InstaConnection.Open();
                    SqlCommand getVendorDetails = new SqlCommand("exec Prc_GetVendorDetailsForOracleCode", InstaConnection);
                    getVendorDetails.CommandTimeout = 18000;
                    SqlDataAdapter adpInstadetail = new SqlDataAdapter(getVendorDetails);
                    DataSet dsVendor = new DataSet();
                    adpInstadetail.Fill(dsVendor);
                    if (dsVendor.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsVendor.Tables[0].Rows)
                        {

                            if (!string.IsNullOrEmpty(Convert.ToString(dr["PANNo"])))
                            {
                                using (Oraconnect = new OracleConnection(oracleconn))
                                {
                                    Oraconnect.Open();
                                    StringBuilder str = new StringBuilder("select APPS.AP_PAN_VENDOR ('" + Convert.ToString(dr["PANNo"]) + "') from dual");
                                    OracleCommand objCmd = new OracleCommand(str.ToString(), Oraconnect);

                                    /*
                                    objCmd.Connection = Oraconnect;
                                    objCmd.CommandText ="APPS.AP_PAN_VENDOR";
                                    objCmd.CommandType = CommandType.StoredProcedure;
                                    //objCmd.Parameters.AddWithValue("PAN", Convert.ToString(dr["PANNo"]));
                                    objCmd.Parameters.Add("PAN", OracleType.VarChar);
                                    objCmd.Parameters["PAN"].Value = Convert.ToString(dr["PANNo"]);
                                    */
                                    DataSet dsOrlVendor = new DataSet();
                                    OracleDataAdapter oralAdp = new OracleDataAdapter(objCmd);
                                    oralAdp.Fill(dsOrlVendor);
                                    if (dsOrlVendor.Tables[0].Rows.Count > 0 && dsOrlVendor.Tables != null)
                                    {
                                        if (!string.IsNullOrEmpty(dsOrlVendor.Tables[0].Rows[0][0].ToString()) && Convert.ToString(dsOrlVendor.Tables[0].Rows[0][0]).Length > 1)
                                        {
                                            string detail = dsOrlVendor.Tables[0].Rows[0][0].ToString();
                                            string[] vDetail = detail.Split('$');

                                            SqlCommand upVendorDetails = new SqlCommand();
                                            upVendorDetails.Connection = InstaConnection;
                                            upVendorDetails.CommandText = "Prc_UpdateOracleCodeWithORacleVendorName";
                                            upVendorDetails.CommandType = CommandType.StoredProcedure;
                                            upVendorDetails.Parameters.Add("@CarVendorId", SqlDbType.Int);
                                            upVendorDetails.Parameters["@CarVendorId"].Value = Convert.ToInt32(dr["CarVendorID"]);
                                            upVendorDetails.Parameters.Add("@OracleCode", SqlDbType.VarChar);
                                            upVendorDetails.Parameters["@OracleCode"].Value = Convert.ToString(vDetail[0]);
                                            upVendorDetails.Parameters.Add("@OracleVendorName", SqlDbType.VarChar);
                                            upVendorDetails.Parameters["@OracleVendorName"].Value = Convert.ToString(vDetail[1]);
                                            upVendorDetails.ExecuteNonQuery();
                                        }
                                    }

                                }
                            }
                        }

                    }
                }

            }

            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }

        }

        private void UploadCarHireDetailsInOracle()
        {
            try
            {

                OracleConnection Oraconnect = null;
                SqlConnection InstaConnection = null;
                string oracleVendorName = string.Empty;
                int status = 0;

                using (InstaConnection = new SqlConnection(Instaconn))
                {
                    InstaConnection.Open();
                    SqlCommand sqlCarHire = new SqlCommand("exec Prc_GetCarHireDetailsForOralceUpload", InstaConnection);
                    sqlCarHire.CommandTimeout = 18000;
                    SqlDataAdapter adpInstadetail = new SqlDataAdapter(sqlCarHire);
                    DataSet dsCarHire = new DataSet();
                    DataSet dsOrlVendor = new DataSet();
                    adpInstadetail.Fill(dsCarHire);
                    if (dsCarHire.Tables[0].Rows.Count > 0)
                    {
                        using (Oraconnect = new OracleConnection(oracleconn))
                        {
                            Oraconnect.Open();
                            foreach (DataRow dr in dsCarHire.Tables[0].Rows)
                            {
                                OracleCommand objCmd = new OracleCommand();
                                objCmd.Connection = Oraconnect;
                                objCmd.CommandText = "APPS.CARHIRE_INSERT";
                                objCmd.CommandType = CommandType.StoredProcedure;

                                objCmd.Parameters.Add("P_CREATION_DATE", OracleType.DateTime);
                                objCmd.Parameters["P_CREATION_DATE"].Value = System.DateTime.Now;//.ToString("dd-MMM-yyyy");

                                objCmd.Parameters.Add("P_CARHIRE_ID", OracleType.Int32);
                                objCmd.Parameters["P_CARHIRE_ID"].Value = Convert.ToInt32(dr["CarHireID"]);

                                objCmd.Parameters.Add("P_PRODUCT", OracleType.VarChar);
                                objCmd.Parameters["P_PRODUCT"].Value = Convert.ToString(dr["Product"]);

                                objCmd.Parameters.Add("P_VENDOR_NAME", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_NAME"].Value = Convert.ToString(dr["Oracle_VendorName"]);

                                objCmd.Parameters.Add("P_VENDOR_SITE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_SITE"].Value = Convert.ToString(dr["VendorSite"]);

                                objCmd.Parameters.Add("P_VENDOR_GSTIN_STATUS", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_GSTIN_STATUS"].Value = Convert.ToString(dr["VendorGSTINStatus"]);

                                objCmd.Parameters.Add("P_VENDOR_GSTIN", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_GSTIN"].Value = Convert.ToString(dr["VendorGSTIN"]);


                                objCmd.Parameters.Add("P_VENDOR_STATE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_STATE"].Value = Convert.ToString(dr["VendorState"]);

                                objCmd.Parameters.Add("P_COR_STATE", OracleType.VarChar);
                                objCmd.Parameters["P_COR_STATE"].Value = Convert.ToString(dr["CorState"]);

                                objCmd.Parameters.Add("P_INVOICE_NUM", OracleType.VarChar);
                                objCmd.Parameters["P_INVOICE_NUM"].Value = Convert.ToString(dr["InvoiceNo"]);

                                objCmd.Parameters.Add("P_INVOICE_DATE", OracleType.DateTime);
                                objCmd.Parameters["P_INVOICE_DATE"].Value = Convert.ToDateTime(dr["InvoiceDate"]).ToString("dd-MMM-yyyy");

                                objCmd.Parameters.Add("P_ACCOUNTING_DATE", OracleType.DateTime);
                                objCmd.Parameters["P_ACCOUNTING_DATE"].Value = System.DateTime.Now.ToString("dd-MMM-yyyy");

                                objCmd.Parameters.Add("P_LOCATION", OracleType.VarChar);
                                objCmd.Parameters["P_LOCATION"].Value = Convert.ToString(dr["Location"]);

                                objCmd.Parameters.Add("P_CARHIRE_AMT", OracleType.Double);
                                objCmd.Parameters["P_CARHIRE_AMT"].Value = dr["CarHireAmt"] == DBNull.Value ? 0 : Convert.ToDouble(dr["CarHireAmt"]);

                                objCmd.Parameters.Add("P_PARKING_AMT", OracleType.Double);
                                objCmd.Parameters["P_PARKING_AMT"].Value = dr["ParkingAmt"] == DBNull.Value ? 0 : Convert.ToDouble(dr["ParkingAmt"]);

                                objCmd.Parameters.Add("P_ENTRY_CHARGES", OracleType.Double);
                                objCmd.Parameters["P_ENTRY_CHARGES"].Value = dr["EntryChargesAmt"] == DBNull.Value ? 0 : Convert.ToDouble(dr["EntryChargesAmt"]);

                                objCmd.Parameters.Add("P_CC_CHARGES", OracleType.Double);
                                objCmd.Parameters["P_CC_CHARGES"].Value = dr["CCChargesAmt"] == DBNull.Value ? 0 : Convert.ToDouble(dr["CCChargesAmt"]);

                                objCmd.Parameters.Add("P_SGST_RATE", OracleType.Double);
                                objCmd.Parameters["P_SGST_RATE"].Value = dr["SGSTPercent"] == DBNull.Value ? 0 : Convert.ToDouble(dr["SGSTPercent"]);

                                objCmd.Parameters.Add("P_CGST_RATE", OracleType.Double);
                                objCmd.Parameters["P_CGST_RATE"].Value = dr["CGSTPercent"] == DBNull.Value ? 0 : Convert.ToDouble(dr["CGSTPercent"]);

                                objCmd.Parameters.Add("P_IGST_RATE", OracleType.Double);
                                objCmd.Parameters["P_IGST_RATE"].Value = dr["IGSTPercent"] == DBNull.Value ? 0 : Convert.ToDouble(dr["IGSTPercent"]);

                                objCmd.Parameters.Add("P_STATUS_TRX", OracleType.Int32);
                                objCmd.Parameters["P_STATUS_TRX"].Value = 0;

                                objCmd.Parameters.Add("P_INSTA_VENDOR_NAME", OracleType.VarChar);
                                objCmd.Parameters["P_INSTA_VENDOR_NAME"].Value = Convert.ToString(dr["Insta_VendorName"]);

                                status = objCmd.ExecuteNonQuery();
                                if (status > 0)
                                {

                                    SqlCommand cmdUploadStatus = new SqlCommand();
                                    cmdUploadStatus.Connection = InstaConnection;
                                    cmdUploadStatus.CommandText = "Prc_UpdateIsOracleUploaded";
                                    cmdUploadStatus.CommandType = CommandType.StoredProcedure;
                                    cmdUploadStatus.Parameters.Add("@InterFaceId", SqlDbType.Int);
                                    cmdUploadStatus.Parameters["@InterFaceId"].Value = Convert.ToInt32(dr["InterfaceID"]);
                                    cmdUploadStatus.ExecuteNonQuery();

                                }
                            }

                        }

                    }
                }

            }

            catch (Exception Ex)
            {

                throw;
            }

        }
        private void UploadCarHireDetailsInOracleForProvision()
        {
            try
            {

                OracleConnection Oraconnect = null;
                SqlConnection InstaConnection = null;
                string oracleVendorName = string.Empty;
                int status = 0;

                using (InstaConnection = new SqlConnection(Instaconn))
                {
                    InstaConnection.Open();
                    //SqlCommand sqlCarHire = new SqlCommand("exec Prc_GetCarHireDetailsForProvisionUpload", InstaConnection);
                    SqlCommand sqlCarHire = new SqlCommand("exec Prc_UploadFinanceCarhirechargesForProvision", InstaConnection);
                    sqlCarHire.CommandTimeout = 18000;
                    SqlDataAdapter adpInstadetail = new SqlDataAdapter(sqlCarHire);
                    DataSet dsCarHire = new DataSet();
                    DataSet dsOrlVendor = new DataSet();
                    adpInstadetail.Fill(dsCarHire);
                    if (dsCarHire.Tables[0].Rows.Count > 0)
                    {
                        using (Oraconnect = new OracleConnection(oracleconn))
                        {
                            Oraconnect.Open();
                            foreach (DataRow dr in dsCarHire.Tables[0].Rows)
                            {
                                OracleCommand objCmd = new OracleCommand();
                                objCmd.Connection = Oraconnect;
                                objCmd.CommandText = "APPS.AP_CARHIRE_PROV_INSERT";
                                objCmd.CommandType = CommandType.StoredProcedure;

                                objCmd.Parameters.Add("P_CREATION_DATE", OracleType.DateTime);
                                objCmd.Parameters["P_CREATION_DATE"].Value = System.DateTime.Now;

                                objCmd.Parameters.Add("P_BOOKING_ID", OracleType.Int32);
                                objCmd.Parameters["P_BOOKING_ID"].Value = Convert.ToInt32(dr["BOOKING_ID"]);

                                objCmd.Parameters.Add("P_CARHIRE_ID", OracleType.Int32);
                                objCmd.Parameters["P_CARHIRE_ID"].Value = Convert.ToInt32(dr["CARHIRE_ID"]);

                                objCmd.Parameters.Add("P_DUTY_TYPE", OracleType.VarChar);
                                objCmd.Parameters["P_DUTY_TYPE"].Value = Convert.ToString(dr["DUTY_TYPE"]);

                                objCmd.Parameters.Add("P_LOCATION_INSTA", OracleType.VarChar);
                                objCmd.Parameters["P_LOCATION_INSTA"].Value = Convert.ToString(dr["LOCATION_INSTA"]);

                                objCmd.Parameters.Add("P_LOCATION_ORACLE", OracleType.VarChar);
                                objCmd.Parameters["P_LOCATION_ORACLE"].Value = Convert.ToString(dr["LOCATION_ORACLE"]);

                                objCmd.Parameters.Add("P_VENDOR_NAME_INSTA", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_NAME_INSTA"].Value = Convert.ToString(dr["VENDOR_NAME_INSTA"]);

                                objCmd.Parameters.Add("P_VENDOR_NAME_ORACLE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_NAME_ORACLE"].Value = Convert.ToString(dr["VENDOR_NAME_ORACLE"]);

                                objCmd.Parameters.Add("P_VENDOR_CODE_ORACLE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_CODE_ORACLE"].Value = Convert.ToString(dr["VENDOR_CODE_ORACLE"]);

                                objCmd.Parameters.Add("P_VENDOR_GST_STATUS", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_GST_STATUS"].Value = Convert.ToString(dr["VENDOR_GST_STATUS"]);


                                objCmd.Parameters.Add("P_VENDOR_GSTIN", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_GSTIN"].Value = Convert.ToString(dr["VENDOR_GSTIN"]);

                                objCmd.Parameters.Add("P_VENDOR_TYPE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_TYPE"].Value = Convert.ToString(dr["VENDOR_TYPE"]);

                                objCmd.Parameters.Add("P_PRODUCT", OracleType.VarChar);
                                objCmd.Parameters["P_PRODUCT"].Value = Convert.ToString(dr["PRODUCT"]);

                                objCmd.Parameters.Add("P_VENDOR_STATE", OracleType.VarChar);
                                objCmd.Parameters["P_VENDOR_STATE"].Value = Convert.ToString(dr["VENDOR_STATE"]);

                                objCmd.Parameters.Add("P_COR_STATE", OracleType.VarChar);
                                objCmd.Parameters["P_COR_STATE"].Value = Convert.ToString(dr["COR_STATE"]);

                                objCmd.Parameters.Add("P_ACCOUNTING_DATE", OracleType.DateTime);
                                objCmd.Parameters["P_ACCOUNTING_DATE"].Value = Convert.ToDateTime(dr["ACCOUNTING_DATE"]).ToString("dd-MMM-yyyy");

                                objCmd.Parameters.Add("P_MONTH", OracleType.VarChar);
                                objCmd.Parameters["P_MONTH"].Value = Convert.ToString(dr["MONTH_Name"]);

                                objCmd.Parameters.Add("P_REVENUE_AMT", OracleType.Double);
                                objCmd.Parameters["P_REVENUE_AMT"].Value = dr["REVENUE_AMT"] == DBNull.Value ? 0 : Convert.ToDouble(dr["REVENUE_AMT"]);

                                objCmd.Parameters.Add("P_CARHIRE_AMT", OracleType.Double);
                                objCmd.Parameters["P_CARHIRE_AMT"].Value = dr["CARHIRE_AMT"] == DBNull.Value ? 0 : Convert.ToDouble(dr["CARHIRE_AMT"]);

                                objCmd.Parameters.Add("P_PARKING_AMT", OracleType.Double);
                                objCmd.Parameters["P_PARKING_AMT"].Value = dr["PARKING_AMT"] == DBNull.Value ? 0 : Convert.ToDouble(dr["PARKING_AMT"]);

                                objCmd.Parameters.Add("P_ENTRY_CHARGES", OracleType.Double);
                                objCmd.Parameters["P_ENTRY_CHARGES"].Value = dr["ENTRY_CHARGES"] == DBNull.Value ? 0 : Convert.ToDouble(dr["ENTRY_CHARGES"]);

                                objCmd.Parameters.Add("P_CC_CHARGES", OracleType.Double);
                                objCmd.Parameters["P_CC_CHARGES"].Value = dr["CC_CHARGES"] == DBNull.Value ? 0 : Convert.ToDouble(dr["CC_CHARGES"]);

                                objCmd.Parameters.Add("P_STATUS_TRX", OracleType.Double);
                                objCmd.Parameters["P_STATUS_TRX"].Value = dr["STATUS_TRX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["STATUS_TRX"]);

                                status = objCmd.ExecuteNonQuery();
                                if (status > 0)
                                {
                                    SqlCommand cmdUploadStatus = new SqlCommand();
                                    cmdUploadStatus.Connection = InstaConnection;
                                    cmdUploadStatus.CommandText = "Prc_UpdateIsOracleUploadedForProvision";
                                    cmdUploadStatus.CommandType = CommandType.StoredProcedure;
                                    cmdUploadStatus.Parameters.Add("@ProvisionID", SqlDbType.Int);
                                    cmdUploadStatus.Parameters["@ProvisionID"].Value = Convert.ToInt32(dr["ProvisionID"]);
                                    cmdUploadStatus.Parameters.Add("@SysUserId", SqlDbType.Int);
                                    cmdUploadStatus.Parameters["@SysUserId"].Value = 1;
                                    cmdUploadStatus.ExecuteNonQuery();

                                }
                            }

                        }

                    }
                }

            }

            catch (Exception Ex)
            {

                throw;
            }

        }

    }
}